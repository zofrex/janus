extern crate janus;
extern crate bincode;
extern crate libc;
extern crate iron;
extern crate persistent;
extern crate handlebars_iron;
extern crate rustc_serialize;
extern crate router;
extern crate urlencoded;
extern crate hyper;
extern crate url;

use iron::prelude::*;
use iron::status;
use iron::typemap::Key;
use iron::Protocol;
use std::net::TcpListener;
use std::io;
use bincode::SizeLimit;
use std::io::Write;

use hyper::net::HttpListener;

use persistent::State;

use bincode::rustc_serialize::encode_into;
use janus::protocol::StartupStatus;
use janus::viewmodels::Configuration;

use handlebars_iron::HandlebarsEngine;
use handlebars_iron::DirectorySource;
use handlebars_iron::Template;
use rustc_serialize::json::ToJson;

use router::Router;
use urlencoded::UrlEncodedBody;

use janus::protocol::LoginRequest;
use std::ops::DerefMut;
use janus::protocol::MessageFromDaemon;
use bincode::rustc_serialize::decode_from;
use janus::protocol::LoginResponse;
use iron::modifiers::Redirect;
use iron::Url;
use janus::protocol::MessageToDaemon;

use url::Url as RawUrl;

struct ConfigurationKey {
}

impl Key for ConfigurationKey {
    type Value = Configuration;
}

struct PipeKey {
}

impl Key for PipeKey {
    type Value = std::io::Stdout;
}

fn main() {
    let listener = TcpListener::bind("0.0.0.0:80");

    drop_privileges();

    let mut stdout = io::stdout();

    let listener = match listener {
        Ok(l) => {
            send(&mut stdout, StartupStatus::Ok);
            l
        }
        Err(ref error) => {
            send(&mut stdout, StartupStatus::BindError(format!("{}", error)));
            std::process::exit(1);
        }
    };

    let config = Configuration::new(String::from("192.168.3.1"));

    let mut chain = Chain::new(routes());
    chain.link_before(State::<ConfigurationKey>::one(config));
    chain.link_before(State::<PipeKey>::one(stdout));
    chain.link_after(handlebars());
    let _i = Iron::new(chain).listen(HttpListener::from(listener), Protocol::http()).unwrap();
}

fn drop_privileges() {
    unsafe {
        libc::setgid(501);
        libc::setuid(501);
    }
}

fn send<T: rustc_serialize::Encodable, W: Write>(out: &mut W, response: T) {
    encode_into(&response, out, SizeLimit::Infinite).expect("janus-httpd: could not send reply");
    out.flush().unwrap();
}

fn hello_world(req: &mut Request) -> IronResult<Response> {
    let mut resp = Response::new();
    let lock = req.get::<State<ConfigurationKey>>().unwrap();
    let config = lock.read().unwrap();
    resp.set_mut(Template::new("index", config.to_json())).set_mut(status::Ok);

    writeln!(&mut std::io::stderr(), "Hello!");
    Ok(resp)
}

fn update(req: &mut Request) -> IronResult<Response> {
    let params = req.get::<UrlEncodedBody>().unwrap();
    let resp = Response::new();

    if let Some(value) = params.get("ip_address") {
        let lock = req.get::<State<ConfigurationKey>>().unwrap();
        let mut config = lock.write().unwrap();
        config.change_ip(value[0].clone());
    }

    Ok(resp)
}

fn login(req: &mut Request) -> IronResult<Response> {
    let params = req.get::<UrlEncodedBody>().unwrap();

    let username = params.get("username").unwrap()[0].clone();
    let password = params.get("password").unwrap()[0].clone();

    let lock = req.get::<State<PipeKey>>().unwrap();
    let mut lock = lock.write().unwrap();
    let mut stdout = lock.deref_mut();
    writeln!(&mut std::io::stderr(), "got stdout!"); // it gets here no problem

    let login_request = MessageToDaemon::LoginRequest(LoginRequest {
        username: username,
        password: password,
    });

    send(&mut stdout, login_request);
    let mut stdin = io::stdin();
    writeln!(&mut std::io::stderr(), "HTTPD: sent login request");

    let message: MessageFromDaemon = decode_from(&mut stdin, SizeLimit::Infinite).unwrap();
    let MessageFromDaemon::LoginResponse(lr) = message;
    writeln!(&mut std::io::stderr(), "Hello!");
    match lr {
        // Gonna get a login response, either redirect back to login page or to success page
        LoginResponse::Ok => redirect_see_other(req, "/success"),
        LoginResponse::WrongUsername => redirect_see_other(req, "/wrong_username"),
        LoginResponse::WrongPassword => redirect_see_other(req, "/wrong_password"),
    }
}

fn redirect_see_other(context: &iron::Request, relative_path: &str) -> IronResult<Response> {
    Ok(Response::with((status::SeeOther,
                       Redirect(absolute_from_relative(&context.url, relative_path)))))
}

fn absolute_from_relative(base: &Url, path: &str) -> Url {
    let base: RawUrl = base.clone().into();
    Url::from_generic_url(base.join(path).unwrap()).unwrap()
}

fn success(_req: &mut Request) -> IronResult<Response> {
    Ok(Response::with((status::Ok, "Login was correct")))
}

fn routes() -> Router {
    let mut router = Router::new();
    router.get("/", hello_world, "index");
    router.post("/change", update, "update");
    router.post("/login", login, "login");
    router.get("/success", success, "success");
    router
}

fn handlebars() -> HandlebarsEngine {
    let mut hbse = HandlebarsEngine::new();
    hbse.add(Box::new(DirectorySource::new("./views/", ".hbs")));

    // load templates from all registered sources
    hbse.reload().unwrap();
    hbse
}
