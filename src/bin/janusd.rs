extern crate bincode;
extern crate janus;

use std::process::Command;
use std::process::Stdio;
use std::process::Child;
use std::process::ChildStdout;

use bincode::SizeLimit;
use bincode::rustc_serialize::decode_from;

use janus::protocol::StartupStatus;

fn main() {
    let mut httpd_process = spawn_httpd();

    {
        // let mut sender = httpd_process.stdin.as_mut().unwrap();
        let mut receiver = httpd_process.stdout.as_mut().unwrap();

        check_startup(receiver);
    }

    httpd_process.wait().expect("janusd: failed waiting on child");
    println!("janusd: quitting...");
}

fn spawn_httpd() -> Child {
    Command::new("janus-httpd")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("janusd: failed to spawn client")
}

fn check_startup(receiver: &mut ChildStdout) {
    let status: StartupStatus = decode_from(receiver, SizeLimit::Infinite)
        .expect("janus-httpd: could not decode startup status");

    match status {
        StartupStatus::BindError(ref msg) => {
            println!("janusd-http: ERROR: failed to bind: {}", msg);
            std::process::exit(1);
        }
        StartupStatus::Ok => println!("janusd: janusd-httpd started successfully"),
    }
}
