extern crate bincode;
extern crate janus;

use std::process::Command;
use std::process::Stdio;
use std::process::Child;
use std::process::ChildStdout;

use bincode::SizeLimit;
use bincode::rustc_serialize::decode_from;
use bincode::rustc_serialize::encode_into;

use janus::protocol::StartupStatus;
use janus::protocol::MessageToDaemon;
use janus::protocol::LoginRequest;
use janus::protocol::LoginResponse;
use janus::protocol::MessageFromDaemon;

use std::io::Write;

fn main() {
    let mut httpd_process = spawn_httpd();

    {
        let mut sender = httpd_process.stdin.as_mut().unwrap();
        let mut receiver = httpd_process.stdout.as_mut().unwrap();

        check_startup(receiver);

        loop {
            println!("DAEMON: In loop");
            let message: MessageToDaemon = decode_from(receiver, SizeLimit::Infinite)
                .expect("fake-janus: received a completely bizzarre message");
            println!("DAEMON: We received a message");

            let response = match message {
                MessageToDaemon::LoginRequest(LoginRequest { username, password }) => {
                    println!("DAEMON: We received a login request");
                    MessageFromDaemon::LoginResponse(check_login(&username, &password))
                }
            };

            encode_into(&response, sender, SizeLimit::Infinite)
                .expect("well what the fuck I don't even know");
            sender.flush().unwrap();
        }
    }

    httpd_process.wait().expect("janusd: failed waiting on child");
    println!("janusd: quitting...");
}

fn check_login(username: &str, password: &str) -> LoginResponse {
    match (username, password) {
        ("root", "guest") => {
            println!("Login is ok");
            LoginResponse::Ok
        }
        ("root", _) => LoginResponse::WrongPassword,
        (_, _) => LoginResponse::WrongUsername,
    }
}

fn spawn_httpd() -> Child {
    Command::new("janus-httpd")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()
        .expect("janusd: failed to spawn client")
}

fn check_startup(receiver: &mut ChildStdout) {
    let status: StartupStatus = decode_from(receiver, SizeLimit::Infinite)
        .expect("janus-httpd: could not decode startup status");

    match status {
        StartupStatus::BindError(ref msg) => {
            println!("janusd-http: ERROR: failed to bind: {}", msg);
            std::process::exit(1);
        }
        StartupStatus::Ok => println!("janusd: janusd-httpd started successfully"),
    }
}
