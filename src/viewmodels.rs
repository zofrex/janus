use rustc_serialize::json::{Json, ToJson};
use std::collections::BTreeMap;

#[derive(RustcEncodable)]
pub struct Configuration {
    ip_address: String,
}

impl Configuration {
    pub fn new(ip_address: String) -> Configuration {
        Configuration { ip_address: ip_address }
    }

    pub fn change_ip(&mut self, ip_address: String) {
        self.ip_address = ip_address;
    }

    fn get_ip(&self) -> String {
        self.ip_address.clone()
    }
}

impl ToJson for Configuration {
    fn to_json(&self) -> Json {
        let mut m = BTreeMap::new();
        m.insert("ip_address".to_string(), self.get_ip());
        m.to_json()
    }
}
