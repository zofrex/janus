extern crate rustc_serialize;

pub mod viewmodels;

pub mod protocol {
    #[derive(RustcEncodable, RustcDecodable, PartialEq)]
    pub enum MessageToDaemon {
        // enum here 'cos we don't know what we're gonna get
        LoginRequest(LoginRequest),
    }

    #[derive(RustcEncodable, RustcDecodable, PartialEq)]
    pub enum MessageFromDaemon {
        // don't necessarily need to enum this - should always know what we're expecting?
        LoginResponse(LoginResponse),
    }

    #[derive(RustcEncodable, RustcDecodable, PartialEq)]
    pub enum StartupStatus {
        Ok,
        BindError(String),
    }

    #[derive(RustcEncodable, RustcDecodable, PartialEq)]
    pub struct LoginRequest {
        pub username: String,
        pub password: String,
    }

    #[derive(RustcEncodable, RustcDecodable, PartialEq)]
    pub enum LoginResponse {
        Ok,
        WrongUsername,
        WrongPassword,
    }
}
